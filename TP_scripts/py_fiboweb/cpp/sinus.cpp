#include <cmath>
#define M_PI 3.14159265358979224

double sinus(double a, double b, double x){
    return std::sin(2 * M_PI * (a * x + b));
}

#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("sinus", &sinus);
    return m.ptr();
}
