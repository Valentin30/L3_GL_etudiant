
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

import fibo
import sinus

xs = range(10)
ys = [fibo.fiboIterative(x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('plot_fibo.png')
plt.clf()

xz = np.linspace(0, 1, 100)
yz = [sinus.sinus(2.0, 0.25, z) for z in xz]
plt.plot(xz, yz)
plt.xlabel('x')
plt.ylabel('sinus(x)')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()
