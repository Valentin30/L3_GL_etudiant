#!bin/sh

#g++ -c imshow.cpp -I/usr/include/opencv2
#g++ -o imshow.out imshow.o -lopencv_core -lopencv_highgui -lopencv_imgcodecs

g++ -c imshow.cpp `pkg-config --cflags opencv`
g++ -o imshow.out imshow.o `pkg-config --libs opencv`
