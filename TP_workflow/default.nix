{ pkgs ? import <nixpkgs> {} }:

with pkgs.python3Packages;

buildPythonPackage {
  name = "pytictactoe";
  src = ./.;
  propagatedBuildInputs = [
    boost
    pkgs.gnome3.gtk
    pkgs.gnome3.gobjectIntrospection
    pygobject3
  ];
}

