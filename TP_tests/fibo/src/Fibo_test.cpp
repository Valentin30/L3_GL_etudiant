#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Fibo) { };

TEST(Fibo,test_fibo_1){
        CHECK_EQUAL(0,fibo(0));
}

TEST(Fibo,test_fibo_2){
        CHECK_EQUAL(1,fibo(1));
}

TEST(Fibo,test_fibo_3){
        CHECK_EQUAL(1,fibo(2));
}

TEST(Fibo,test_fibo_4){
        CHECK_EQUAL(2,fibo(3));
}

TEST(Fibo,test_fibo_5){
        CHECK_EQUAL(3,fibo(4));
}

